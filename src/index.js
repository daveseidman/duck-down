// TODO: reexport all models with compression

import { WebGLRenderer, Scene, PerspectiveCamera,
  AmbientLight, DirectionalLight, BufferGeometry, BufferAttribute, Raycaster,
  CubeTextureLoader, Fog, FogExp2, Frustum,
  Mesh, Object3D, BoxGeometry, LinearFilter, MeshBasicMaterial, MeshNormalMaterial, MeshLambertMaterial, InstancedMesh,
  Vector2, Vector3, Color, Quaternion, Matrix4,
  Points, PointsMaterial } from 'three';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
// import { BokehPass } from 'three/examples/jsm/postprocessing/BokehPass';
// import { EffectPass } from 'three/exmaples/jsm/postprocessing/EffectPass';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import * as dat from 'dat.gui';
import './index.scss';
import autoBind from 'auto-bind';
import Stats from 'stats-js';
import { Tween, Easing, update as updateTweens } from '@tweenjs/tween.js';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader';
import { createEl, addEl } from 'lmnt';
import lyrics from './assets/lyrics.json';
import px2 from './assets/images/gradient_1.png';
import nx2 from './assets/images/gradient_2.png';
import py2 from './assets/images/gradient_3.png';
import ny2 from './assets/images/gradient_4.png';
import pz2 from './assets/images/gradient_5.png';
import nz2 from './assets/images/gradient_6.png';

const draco = new DRACOLoader();
draco.setDecoderPath('./assets/draco/');

// const degToRad = deg => deg * (Math.PI / 180);

class App {
  constructor() {
    autoBind(this);

    this.config = {

    };

    this.stats = new Stats();
    this.stats.showPanel(0);
    if (window.location.hostname === 'localhost') document.body.appendChild(this.stats.dom);

    this.readout = createEl('div', { className: 'readout' });
    addEl(this.readout);
    this.setupAudio().then(this.setupScene).then(this.render);

    window.addEventListener('resize', this.resize);
  }

  setupConfig() {
    // const gui = new dat.GUI();
    // gui.add(this.config.streets, 'bothSides').name('park on both sides').onChange(this.placeCars);
    // gui.open();
  }

  setupAudio() {
    return new Promise((resolve) => {
      const URL = './assets/audio/duck-down.mp3';

      // let audioBuffer;

      this.audioContext = new AudioContext();
      this.analyser = this.audioContext.createAnalyser();
      this.analyser.fftSize = 2048;
      const bufferLength = this.analyser.frequencyBinCount;
      this.dataArray = new Uint8Array(bufferLength);

      this.state = {
        playing: false,
      };


      fetch(URL)
        .then(response => response.arrayBuffer())
        .then(arrayBuffer => this.audioContext.decodeAudioData(arrayBuffer))
        .then((audioBuffer) => {
          // audioBuffer = _audioBuffer;

          this.source = this.audioContext.createBufferSource();
          this.source.buffer = audioBuffer;
          this.source.connect(this.analyser);
          this.source.connect(this.audioContext.destination);

          const playButton = createEl('button', { className: 'audio-play', innerText: 'Play' }, {}, { click: () => {
            this.state.playing = !this.state.playing;
            playButton.innerText = this.state.playing ? 'Pause' : 'Play';
            this.source[this.state.playing ? 'start' : 'stop']();
          } });
          addEl(playButton);
        });
      return resolve();
    });
  }

  setupScene() {
    return new Promise((resolve) => {
      this.scene = new Scene();
      // this.scene.fog = new FogExp2(0x7c91a3, 0.05);
      this.fog = new Fog(0x7c91a3, 0, 100);
      this.scene.fog = this.fog;
      this.camera = new PerspectiveCamera(20, window.innerWidth / window.innerHeight, 0.1, 100);
      this.camera.position.z = 10;
      // this.camHeight = 120;
      // this.camera.position.y = this.camHeight;
      this.renderer = new WebGLRenderer({ antialias: true });
      this.renderer.setSize(window.innerWidth, window.innerHeight);
      this.composer = new EffectComposer(this.renderer);
      this.composer.addPass(new RenderPass(this.scene, this.camera));
      document.body.appendChild(this.renderer.domElement);

      this.sun = new DirectionalLight(0xffffff, 1);
      this.sun.target.position.set(-3, -10, 2);
      this.scene.add(this.sun);

      this.pointsGeo = new BufferGeometry();
      this.positions = new Float32Array(1024 * 3);
      for (let i = 0; i < this.positions.length; i += 3) {
        this.positions[i] = i * 0.025 - 4;
      }
      this.pointsGeo.setAttribute('position', new BufferAttribute(this.positions, 3));
      this.points = new Points(this.pointsGeo, new PointsMaterial({ size: 0.25 }));

      this.scene.add(this.points);

      this.controls = new OrbitControls(this.camera, this.renderer.domElement);
      this.controls.enableDamping = true;

      new CubeTextureLoader().load([px2, nx2, py2, ny2, pz2, nz2], (texture) => {
        this.scene.background = texture;
        return resolve();
      });
    });
  }

  render() {
    this.stats.begin();
    updateTweens();
    this.composer.render();

    this.analyser.getByteTimeDomainData(this.dataArray);
    this.readout.innerText = this.dataArray.length;

    for (let i = 0; i < this.dataArray.length; i += 1) {
      this.positions[(i * 3) + 1] = this.dataArray[i] / 128;
    }
    this.points.geometry.attributes.position.needsUpdate = true;

    this.stats.end();

    requestAnimationFrame(this.render);
  }


  resize() {
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
  }
}

const app = new App();
if (window.location.hostname === 'localhost') window.app = app;
